import 'package:flutter/material.dart';
import 'package:flutter_form/base/base_view.dart';
import 'package:flutter_form/src/auth/views_models/auth_model.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login')),
      body: BaseView<AuthFormViewModel>(
        model: AuthFormViewModel(),
        builder: (context, model, child) {
          return SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: model.nameController,
                  decoration: InputDecoration(hintText: "Username"),
                ),
                TextField(
                  controller: model.passController,
                  obscureText: model.obscureText,
                  decoration: InputDecoration(
                      hintText: "Password",
                      suffix: GestureDetector(
                        onTap: model.passwordToggle,
                        child: Icon(model.obscureText
                            ? Icons.remove_red_eye_outlined
                            : Icons.remove_red_eye),
                      )),
                ),
                ElevatedButton(onPressed: model.submit, child: Text('Submit'))
              ],
            ),
          );
        },
      ),
    );
  }
}
