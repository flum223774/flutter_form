import 'package:flutter/material.dart';
import 'package:flutter_form/base/base_model.dart';
import 'package:validators/validators.dart' as validate;
import 'package:fluttertoast/fluttertoast.dart';

class AuthFormViewModel extends BaseModel {
  bool _obscureText = true;
  TextEditingController nameController = TextEditingController();
  TextEditingController passController = TextEditingController();

  bool get obscureText => _obscureText;

  passwordToggle() {
    _obscureText = !_obscureText;
    notifyListeners();
  }

  submit() {
    print(nameController.text);
    print(passController.text);
    String? _smg;
    if (!validate.isLength(nameController.text, 5)) {
      _smg = 'Tên quá ngắn';
    }
    if (_smg != null) {
      print(_smg);
    }
  }
}
